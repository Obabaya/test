﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IIterludeState : ISceneState
{
    //下一個場景狀態
    protected ISceneState m_NextState;

    //下一個場景名稱
    protected string m_NextScene;

    public IIterludeState(SceneStateController Controller) :base(Controller)
    {
        StateName = "IIterludeState";
    }

    public void SetNextState(ISceneState NextState, string NextScene)
    {
        m_NextState = NextState;
        m_NextScene = NextScene;
    }
}
