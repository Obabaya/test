﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// SceneState必繼承類別
/// </summary>
public class ISceneState
{

    //狀態名稱
    private string m_StateName = "IStateName";
    
    public string StateName { get => m_StateName; set => m_StateName = value; }

    //控制者
    protected SceneStateController m_Controller = null;

    public ISceneState(SceneStateController Controller)
    {
        m_Controller = Controller;
    }

    //狀態開始(初始化用)
    public virtual void StateBegin()
    { }

    //狀態結束(資源釋放)
    public virtual void StateEnd()
    { }

    //更新
    public virtual void StateUpdate()
    { }

    public override string ToString()
    {
        return string.Format("[IStateScene : {0}]", m_StateName);
    }
}
