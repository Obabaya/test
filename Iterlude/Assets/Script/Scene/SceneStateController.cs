﻿#define DEBUG  

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// 場景狀態控制者(切換場景用)
/// </summary>
public class SceneStateController
{
    /// <summary>
    /// 狀態是否開始
    /// </summary>
    private bool m_IsBegin = false;
    private ISceneState m_State;

    AsyncOperation SceneAsync;

    public SceneStateController()
    { }

    /// <summary>
    /// 設定場景狀態(無過場)
    /// </summary>
    /// <param name="State">場景狀態物件</param>
    /// <param name="LoadSceneName">場景名稱</param>
    public void SetState(ISceneState State, string LoadSceneName)
    {
        m_IsBegin = false;

        // 載入場景
        LoadScene(LoadSceneName);

        // 通知前一個State結束
        if (m_State != null)
            m_State.StateEnd();

        // 設定
        m_State = State;
    }

    //場景載入
    private void LoadScene(string LoadSceneName)
    {
        if (LoadSceneName == null || LoadSceneName.Length == 0)
            return;
        SceneAsync = SceneManager.LoadSceneAsync(LoadSceneName);
    }

    /// <summary>
    /// 設定場景狀態(有過場)
    /// </summary>
    /// <param name="NextState">下一個場景狀態</param>
    /// <param name="NextSceneName">下一個場景名稱</param>
    /// <param name="IterludeState">過場狀態</param>
    /// <param name="IterludeSceneName">過場名稱</param>
    public void SetState(ISceneState NextState, string NextSceneName, IIterludeState IterludeState, string IterludeSceneName)
    {
        IterludeState.SetNextState(NextState, NextSceneName);
        SetState(IterludeState, IterludeSceneName);
    }

    // 更新
    public void StateUpdate()
    {
        /*if (m_State.StateName != "FirstState" && !SceneAsync.isDone)
            Debug.Log(SceneAsync.progress.ToString());*/
        if (m_State.StateName != "FirstState" && !SceneAsync.isDone)
            return;

        // 通知新的State開始
        if (m_State != null && m_IsBegin == false)
        {
            m_State.StateBegin();
            m_IsBegin = true;
        }

        if (m_State != null)
            m_State.StateUpdate();
    }
}
